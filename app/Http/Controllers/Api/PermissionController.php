<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PermissionCollection;
use App\Http\Resources\PermissionResource;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;


class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return PermissionResource
     */
    public function index(Request $request)
    {

        abort_if(Gate::denies('permission_view') , Response::HTTP_FORBIDDEN, '403 Forbidden');

        if($request->getAll) {
            $query = Permission::orderBy('name', 'asc')->get();
        }else{
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = intVal($request->currentpage);

            $query = Permission::where('name', 'LIKE', "%$searchValue%")
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }

        return new PermissionResource($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        abort_if(Gate::denies('permission_create') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->validate([
            'name' => 'required|unique:permissions,name|regex:/(^[A-Za-z0-9-_]+$)+/',
        ]);

        $permission = Permission::create(['guard_name' => 'web', 'name' => $request->name]);

        if ($permission) {
            return response()->json($permission);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Permission $permission
     * @return PermissionResource
     */
    public function edit(Permission $permission)
    {
        abort_if(Gate::denies('permission_edit') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        return new PermissionResource($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        abort_if(Gate::denies('permission_edit') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request->validate([
            'name' => 'required|unique:permissions,name,' . $request->permission . '|regex:/(^[A-Za-z0-9-_]+$)+/',
        ]);
        $permission = Permission::find($id);
        $permission->name = $request->name;
        $permission->save();

        if ($permission) {
            return response()->json($permission);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        abort_if(Gate::denies('permission_edit') , Response::HTTP_FORBIDDEN, '403 Forbidden');
        $permission = Permission::findOrFail($id);
        $permission->delete();

        if ($permission) {
            return response()->json($permission);
        }
    }
}
